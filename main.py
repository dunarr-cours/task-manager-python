
from datetime import datetime

class Task:
    def __init__(self, content):
        self.content = content
        self.state = False
        self.created_at = datetime.now()

class TaskManager:
    def __init__(self):
        self.tasks = []

    def add_task(self):
        task_text = input("Nom de la tâche: ")
        task = Task(task_text)
        self.tasks.append(task)

    def remove_task(self):
        try:
            task_id = int(input("Id de la tâche à supprimer: "))
            if 0 <= task_id < len(self.tasks):
                self.tasks.pop(task_id)
            else:
                print("Veuillez rentrer un nombre entre 0 et %i" % (len(self.tasks) - 1))
        except ValueError:
            print("Veuillez rentrer un nombre entre 0 et %i" % (len(self.tasks) - 1))


    def validate_task(self):
        task_id = int(input("Id de la tâche à valider: "))
        # task = self.tasks[task_id]
        # task.state = True
        self.tasks[task_id].state = True

    def list_tasks(self):
        template = "{id} - {date} - {content} [{state}]"
        for task in self.tasks:
            index = self.tasks.index(task)
            print(template.format(
                id=index,
                date=task.created_at,
                content=task.content,
                state=task.state
            ))

class UserInterface:
    def __init__(self):
        self.manager = TaskManager()
        self.loop = True

    def run(self):

        functions = {
            "add": self.manager.add_task,
            "remove": self.manager.remove_task,
            "validate": self.manager.validate_task,
            "list": self.manager.list_tasks,
            "exit": self.exit,
            "help": self.display_help
        }

        self.loop = True
        while self.loop:
            user_input = input("Que voulez-vous faire?\n")

            if(user_input in functions):
                functions[user_input]()
            else:
                self.display_help()

            # if user_input == "add":
            #     self.manager.add_task()
            # elif user_input == "remove":
            #     self.manager.remove_task()
            # elif user_input == "validate":
            #     self.manager.validate_task()
            # elif user_input == "list":
            #     self.manager.list_tasks()
            # elif user_input == "exit":
            #     self.exit()
            # else:
            #     self.display_help()


    def display_help(self):
        print('''Aide:
 - list     -> liste les tâches
 - remove   -> supprimer une tâche
 - add      -> ajoute une nouvelle tâche
 - validate -> valider une tâche
 - help     -> Affiche ce message d'aide
 - exit     -> sortir du programme''')

    def exit(self):
        self.loop = False


# manager = TaskManager()
# manager.add_task()
# manager.add_task()
# manager.add_task()
# manager.remove_task()
# manager.validate_task()
# manager.list_tasks()

interface = UserInterface()
interface.run()
